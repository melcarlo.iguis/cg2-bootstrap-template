import React, { Component, Suspense, useState, useEffect } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './scss/style.scss'
// axios api
import api from './api/api'
import { AppProvider } from './AppContext'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'))
const Register = React.lazy(() => import('./views/pages/register/Register'))
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'))
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'))
// global state and variable

function App() {
  const [tenantList, setTenantList] = useState([])
  const [incidentReportList, setIncidentReportList] = useState([])
  const [historyList, setHistoryList] = useState([])
  const [dietaryData, setDietaryData] = useState([{}])
  const [dailylogData, setDailylogData] = useState([])
  const [action, setAction] = useState('')
  const [diagnosisData, setDiagnosisData] = useState([{}])
  const [behaviorData, setBehaviorData] = useState([{}])
  const [shiftLogData, setShiftLogData] = useState([{}])
  const [vitalData, setVitalData] = useState([])
  // useState for closing dialog automatically
  const [dialogClose, setDialogClose] = useState(false)
  const [isEmergencyOpen, setIsEmergencyOpen] = useState(false)

  const [actionValue, setActionValue] = useState(0)

  const [numberOfShiftLog, setNumberOfShiftLog] = useState(0)

  const [user, setUser] = useState({
    id: null,
    user_type: null,
  })

  const [allergiesList, setAllergiesList] = useState([{}])

  useEffect(() => {
    let token = localStorage.getItem('token')
    api
      .get('/users/details', {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((result) => {
        if (typeof result.data._id !== 'undefined') {
          setUser({
            id: result.data._id,
            user_type: result.data.user_type,
          })
        } else {
          setUser({
            id: null,
            user_type: null,
          })
        }
      })
  }, [])

  return (
    <AppProvider
      value={{
        tenantList,
        setTenantList,
        dialogClose,
        setDialogClose,
        action,
        setAction,
        user,
        setUser,
        allergiesList,
        setAllergiesList,
        incidentReportList,
        setIncidentReportList,
        historyList,
        setHistoryList,
        isEmergencyOpen,
        setIsEmergencyOpen,
        diagnosisData,
        setDiagnosisData,
        dietaryData,
        setDietaryData,
        dailylogData,
        setDailylogData,
        vitalData,
        setVitalData,
        actionValue,
        setActionValue,
        behaviorData,
        setBehaviorData,
        numberOfShiftLog,
        setNumberOfShiftLog,
        shiftLogData,
        setShiftLogData,
      }}
    >
      <BrowserRouter>
        <Suspense fallback={loading}>
          <Routes>
            <Route exact path="/login" name="Login Page" element={<Login />} />
            <Route exact path="/register" name="Register Page" element={<Register />} />
            <Route exact path="/404" name="Page 404" element={<Page404 />} />
            <Route exact path="/500" name="Page 500" element={<Page500 />} />
            <Route path="*" name="Home" element={<DefaultLayout />} />
          </Routes>
        </Suspense>
      </BrowserRouter>
    </AppProvider>
  )
}

export default App
