import React, { useContext, useEffect, useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import SosIcon from '@mui/icons-material/Sos'
// global variable
import AppContext from '../AppContext'
import api from '../api/api'
import Form from 'react-bootstrap/Form'
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilSpeedometer,
  cilEnvelopeOpen,
  cilList,
  cilMenu,
  cilSearch,
  cilHome,
  cilHeart,
  cilNotes,
  cilPeople,
} from '@coreui/icons'

import { AppBreadcrumb } from './index'
import { AppHeaderDropdown } from './header/index'
import { logo } from 'src/assets/brand/logo'

const AppHeader = () => {
  const location = useLocation()
  const dispatch = useDispatch()
  const sidebarShow = useSelector((state) => state.sidebarShow)
  const {
    tenantList,
    setTenantList,
    actionValue,
    setActionValue,
    numberOfShiftLog,
    setNumberOfShiftLog,
    shiftLogData,
    setShiftLogData,
  } = useContext(AppContext)

  const [wordEntered, setWordEntered] = useState('')

  // function for search input
  const handleSearchFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get('/tenants/fetch', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setTenantList(res.data)

        const newFilter = res.data.filter((value) => {
          let fullName = value.firstName + ' ' + value.middleName + ' ' + value.lastName

          return fullName.toLowerCase().includes(searchWord.toLowerCase())
        })
        setTenantList(newFilter)
      })
  }

  return (
    <CHeader position="sticky" className="mb-4">
      <CContainer fluid>
        <CHeaderToggler
          className="ps-1"
          onClick={() => dispatch({ type: 'set', sidebarShow: !sidebarShow })}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none" to="/">
          <CIcon icon={logo} height={48} alt="Logo" />
        </CHeaderBrand>
        <CHeaderNav className="d-none d-md-flex me-auto">
          <CNavItem className={location.pathname == '/' ? 'header-item-active' : 'header-item'}>
            <CNavLink className="d-none d-md-flex" href="/">
              <CIcon className="sidebar-brand-full" icon={cilHome} height={23} />
              Home
            </CNavLink>
          </CNavItem>
          <CNavItem
            className={location.pathname == '/sos' ? 'header-item-active ml-3' : 'header-item ml-3'}
          >
            <CNavLink className="d-none d-md-flex" href="/">
              SOS
            </CNavLink>
          </CNavItem>
          <CNavItem className="seach-bar-div">
            <Form.Control
              value={wordEntered}
              onChange={handleSearchFilter}
              type="text"
              placeholder="Search Resident"
              className={location.pathname == '/' ? '' : 'disable-search'}
            />
          </CNavItem>
        </CHeaderNav>
        <CHeaderNav>
          <CNavItem
            className={location.pathname == '/shiftlog' ? 'header-item-active' : 'header-item'}
          >
            <CNavLink href="/shiftlog">
              <CIcon className="mr-2" icon={cilNotes} size="lg" />
              Shift Summary Log
            </CNavLink>
          </CNavItem>
        </CHeaderNav>
        <CHeaderNav className="ms-3">
          <AppHeaderDropdown id="drop-down-header" />
        </CHeaderNav>
      </CContainer>
      {/*<CHeaderDivider />*/}
      {/*<CContainer fluid><AppBreadcrumb /></CContainer>*/}
    </CHeader>
  )
}

export default AppHeader
