import React, { useState, useEffect, useContext, useRef } from 'react'
import { Row, Col, Form, Table } from 'react-bootstrap'
import '../../App.css'
import { Grid, Box, CardActionArea, CardActions } from '@mui/material'
import {
  faBedPulse,
  faBurger,
  faClipboard,
  faFaceGrin,
  faHandDots,
  faHeartPulse,
  faPersonFalling,
  faUserDoctor,
  faBell,
  faTriangleExclamation,
  faCircleInfo,
  faPhone,
  faHospital,
  faHospitalUser,
  faCalendarCheck,
  faCalendarPlus,
  faCrutch,
  faPeopleGroup,
  faFilePdf,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Button from '@mui/material/Button'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardGroup,
  CCardImage,
  CCardLink,
  CCardSubtitle,
  CCardText,
  CCardTitle,
  CListGroup,
  CListGroupItem,
  CNav,
  CNavItem,
  CNavLink,
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
} from '@coreui/react'
import Card from '@mui/material/Card'
import AddIcon from '@mui/icons-material/Add'
import PendingActionsIcon from '@mui/icons-material/PendingActions'
import AddBoxIcon from '@mui/icons-material/AddBox'
import TextSnippetIcon from '@mui/icons-material/TextSnippet'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
import FolderSharedIcon from '@mui/icons-material/FolderShared'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import { CContainer, CRow, CCol, CCardHeader } from '@coreui/react'
import { styled } from '@mui/system'
import SwitchUnstyled, { switchUnstyledClasses } from '@mui/base/SwitchUnstyled'

import Menu from '@mui/material/Menu'
import HomeIcon from '@mui/icons-material/Home'
import DateRangeIcon from '@mui/icons-material/DateRange'
import PersonSearchIcon from '@mui/icons-material/PersonSearch'
import RestaurantIcon from '@mui/icons-material/Restaurant'
import EditLocationAltIcon from '@mui/icons-material/EditLocationAlt'
import MenuItem from '@mui/material/MenuItem'
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state'
import NoteAltIcon from '@mui/icons-material/NoteAlt'
import InsertChartIcon from '@mui/icons-material/InsertChart'
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft'
import DriveFileMoveIcon from '@mui/icons-material/DriveFileMove'
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt'
import LocalPharmacyIcon from '@mui/icons-material/LocalPharmacy'
import PersonIcon from '@mui/icons-material/Person'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import InventoryIcon from '@mui/icons-material/Inventory'
import VolunteerActivismIcon from '@mui/icons-material/VolunteerActivism'
import AssessmentIcon from '@mui/icons-material/Assessment'
import ContentPasteSearchIcon from '@mui/icons-material/ContentPasteSearch'
import FindInPageIcon from '@mui/icons-material/FindInPage'
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import WarningIcon from '@mui/icons-material/Warning'
import FeedIcon from '@mui/icons-material/Feed'
import VaccinesIcon from '@mui/icons-material/Vaccines'
// facesheet code
import { useReactToPrint } from 'react-to-print'

// import FaceSheet  from './FaceSheet';

import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'

// global variable
import AppContext from '../../AppContext'

import ListAltIcon from '@mui/icons-material/ListAlt'

// axios api
import api from '../../api/api'

// components to include
import Allergies from './include/Allergies'
import Incident from './include/Incident'
import Behavior from './include/Behavior'
import Dailylog from './include/Dailylog'
import Diagnosis from './include/Diagnosis'
import Dietary from './include/Dietary'
import Vital from './include/Vital'
import Profile from './include/Profile'
import BasicInfo from './form/BasicInfo'

// tab panel
import PropTypes from 'prop-types'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Typography from '@mui/material/Typography'

import { useNavigate } from 'react-router-dom'

import * as ReactBootstrap from 'react-bootstrap'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

// skeleton
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

import FormControlLabel from '@mui/material/FormControlLabel'
import MedicationIcon from '@mui/icons-material/Medication'
// content loader / skeleton
import ContentLoader from 'react-content-loader'
const blue = {
  500: '#007FFF',
}

const grey = {
  400: '#BFC7CF',
  500: '#AAB4BE',
  600: '#6F7E8C',
}

const Root = styled('span')(
  ({ theme }) => `
  font-size: 0;
  position: relative;
  display: inline-block;
  width: 40px;
  height: 20px;
  margin: 10px;
  cursor: pointer;

  &.${switchUnstyledClasses.disabled} {
    opacity: 0.4;
    cursor: not-allowed;
  }

  & .${switchUnstyledClasses.track} {
    background: ${theme.palette.mode === 'dark' ? grey[600] : grey[400]};
    border-radius: 10px;
    display: block;
    height: 100%;
    width: 100%;
    position: absolute;
  }

  & .${switchUnstyledClasses.thumb} {
    display: block;
    width: 14px;
    height: 14px;
    top: 3px;
    left: 3px;
    border-radius: 16px;
    background-color: #fff;
    position: relative;
    transition: all 200ms ease;
  }

  &.${switchUnstyledClasses.focusVisible} .${switchUnstyledClasses.thumb} {
    background-color: ${grey[500]};
    box-shadow: 0 0 1px 8px rgba(0, 0, 0, 0.25);
  }

  &.${switchUnstyledClasses.checked} {
    .${switchUnstyledClasses.thumb} {
      left: 22px;
      top: 3px;
      background-color: #fff;
    }

    .${switchUnstyledClasses.track} {
      background: ${blue[500]};
    }
  }

  & .${switchUnstyledClasses.input} {
    cursor: inherit;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    opacity: 0;
    z-index: 1;
    margin: 0;
  }
  `,
)

function TenantProfile() {
  let x = localStorage.getItem('actionValue')
  let y = parseInt(x)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const open = Boolean(anchorEl)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = (val) => {
    setAnchorEl(null)
    console.log(val)
  }

  const helloWorld = () => {
    console.log('hehehe')
  }

  // tab panel
  function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    )
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    }
  }

  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const label = { componentsProps: { input: { 'aria-label': 'Demo switch' } } }
  let navigate = useNavigate()
  const componentRef = useRef()

  const [isReachTheEndData, setIsReachTheEndData] = useState(false)
  const [isReachTheStartData, setIsReachTheStartData] = useState(false)

  const [profileSubMenu, setProfileSubMenu] = useState('Basic Information')

  const [currentIndex, setCurrentIndex] = useState(0)

  const [activeTab, setActiveTab] = useState(0)

  // usestate for loading
  const [loading, setLoading] = useState(false)
  // usestate for privacy
  const [isPrivacyOn, setIsPrivacyOn] = useState(false)
  const delayTimer = () => {
    const timer = setTimeout(() => {
      handlePrint()
    }, 500)
    return () => clearTimeout(timer)
  }
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  })
  const [tenantData, setTenantData] = useState({})
  const [currentTab, setCurrentTab] = useState('')
  const [age, setAge] = useState('')
  const {
    diagnosisData,
    setDiagnosisData,
    allergiesList,
    setAllergiesList,
    dialogClose,
    setDialogClose,
    tenantList,
    setTenantList,
    behaviorData,
    setBehaviorData,
    dailylogData,
    setDailylogData,
    dietaryData,
    setDietaryData,
    incidentReportList,
    setIncidentReportList,
    vitalData,
    setVitalData,
  } = useContext(AppContext)

  const [statusInput, setStatusInput] = useState('')
  let name = localStorage.getItem('name')
  let token = localStorage.getItem('token')
  let tenantName = localStorage.getItem('tenantName')
  let tenantId = localStorage.getItem('tenantId')

  const firstUpdate = useRef(true)
  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      handleUpdateStatus()
    }
  }, [statusInput])

  // function for updating tenant's status
  const handleUpdateStatus = () => {
    console.log('changing status. .. .')
    api
      .put(
        `tenants/${tenantId}/update`,
        { status: statusInput },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then((result) => {
        // tenantData.forEach(item => {
        //      if(item._id === tenantId){
        //      item.status = statusInput
        //      }
        //    })

        // setting the new data to the state
        setTenantData({ ...tenantData, status: statusInput })

        toast.success('Updated Successfully', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: false,
          draggable: true,
          progress: undefined,
        })
        // adding history to database
        const input2 = {
          title: `Edited ${tenantName}'s status to ${statusInput}`,
          tenantName: tenantName,
          tenantId: tenantId,
          userName: name,
        }
        api
          .post(`/history/create/`, input2, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)
          })
          .catch((err) => {
            console.error(err)
          })

        // delay function

        setTimeout(function () {
          setDialogClose(false)
        }, 2000)
      })
      .catch((err) => {
        console.error(err)
      })
  }

  const fetchTenantHistory = async () => {
    await api
      .get(`/history/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        // setHistoryList(res.data)
      })
  }

  useEffect(() => {
    fetchTenantHistory()
  }, [])

  let fullName = tenantData.firstName + ' ' + tenantData.middleName + ' ' + tenantData.lastName

  useEffect(() => {
    setCurrentTab('profile')
    // localStorage.setItem('tenantId' , tenantData._id);
    // localStorage.setItem('tenantName' , fullName);
  }, [])
  // code for fetching allergy

  const cutBirthday = (string) => {
    let date = new Date(string)
    const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    let fullDate = month + ' ' + day + ', ' + year
    return fullDate
  }

  const getTheAge = (birthday) => {
    let date = new Date(birthday)
    let currentYear = new Date().getFullYear()
    let year = date.getUTCFullYear()

    const age = currentYear - year

    return age
  }

  // function for fetchning the name of the tenant and setting it to the localstorage
  const setTenantName = (id) => {
    api
      .get(`/tenants/${id}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        let fullName = res.data.firstName + ' ' + res.data.middleName + ' ' + res.data.lastName

        localStorage.setItem('tenantName', fullName)
      })
  }

  useEffect(() => {
    fetchBehaviorData()
  }, [tenantId])

  const fetchBehaviorData = () => {
    let token = localStorage.getItem('token')
    api
      .get(`/tenants/behavior/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        const newFilter = res.data.filter((val) => {
          return val.isActive === true
        })
        setBehaviorData(newFilter)
      })
  }

  const fetchAllergy = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/tenants/allergy/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  useEffect(() => {
    const getAllergyList = async () => {
      const allAlergy = await fetchAllergy()
      if (allAlergy) setAllergiesList(allAlergy)
    }

    getAllergyList()
  }, [tenantId])

  const fetchTenant = async () => {
    let tenantId = localStorage.getItem('tenantId')
    let token = localStorage.getItem('token')
    const res = await api.get(`/tenants/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  useEffect(() => {
    const getTenantData = async () => {
      const tenantDetails = await fetchTenant()
      if (tenantDetails) setTenantData(tenantDetails)
    }

    getTenantData()
  }, [tenantId])

  const fetchDiagnosisData = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    const res = await api.get(`/tenants/diagnosis/${tenantId}/fetch`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  }

  useEffect(() => {
    const getDiagnosisData = async () => {
      const allDiagnosis = await fetchDiagnosisData()
      if (allDiagnosis) setDiagnosisData(allDiagnosis)
    }

    getDiagnosisData()
  }, [tenantId])

  const changeCurrentTab = (profileVal, subMenu) => {
    setCurrentTab('profile')
    // setProfileTab(profileVal)
    setProfileSubMenu(subMenu)
  }

  useEffect(() => {
    let tenantCurrentIndex = localStorage.getItem('tenantCurrentIndex')
    if (tenantList.length - 1 == tenantCurrentIndex) {
      setIsReachTheEndData(true)
    } else {
      setIsReachTheEndData(false)
    }

    if (tenantCurrentIndex == 0) {
      setIsReachTheStartData(true)
    } else {
      setIsReachTheStartData(false)
    }
  }, [currentIndex])
  // function for next and previous button
  const nextButtonFunction = () => {
    const search = (obj) => obj._id === tenantId
    setLoading(false)
    const index = tenantList.findIndex(search)

    localStorage.setItem('tenantCurrentIndex', index + 1)
    const newIndex = tenantList[index + 1]._id
    setTenantName(newIndex)
    // set new id in the localstorage
    localStorage.setItem('tenantId', newIndex)
    navigate(`/resident/${newIndex}`)
    fetchTenant(newIndex)
    setCurrentIndex(index + 1)
    fetchAllergy()
  }

  const prevButtonFunction = () => {
    const search = (obj) => obj._id === tenantId
    setLoading(false)
    const index = tenantList.findIndex(search)

    localStorage.setItem('tenantCurrentIndex', index - 1)
    const newIndex = tenantList[index - 1]._id

    // set new id in the localstorage
    localStorage.setItem('tenantId', newIndex)
    navigate(`/resident/${newIndex}`)
    fetchTenant(newIndex)
    setCurrentIndex(index - 1)
    fetchAllergy()
  }

  const formatDate = (string) => {
    if (string === undefined) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' })
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  const fetchDailylogData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/observation/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setDailylogData(res.data)
      })
  }

  useEffect(() => {
    fetchDailylogData()
  }, [tenantId])

  const fetchDietaryData = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get(`/tenants/dietary/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res.data)
        setDietaryData(res.data)
      })
  }

  useEffect(() => {
    fetchDietaryData()
  }, [tenantId])

  const fetchIncidentReport = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/incident/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setIncidentReportList(res.data)
      })
  }

  useEffect(() => {
    fetchIncidentReport()
  }, [tenantId])

  const fetchVitalData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    api
      .get(`/vitals/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setVitalData(res.data)
      })
  }

  useEffect(() => {
    fetchVitalData()
  }, [tenantId])

  console.log(vitalData)

  const changeActiveTab = (activeTab) => {
    setActiveTab(activeTab)
    scrollDown()
  }

  const resetActiveTab = () => {
    setActiveTab(0)
  }

  const scrollDown = () => {
    window.scrollTo({
      top: 1000,
      behavior: 'smooth',
    })
  }

  return (
    <div>
      {isPrivacyOn == false ? (
        <div>
          <div
            onClick={() => nextButtonFunction()}
            id={isReachTheEndData ? 'hide-element' : 'next-btn'}
          >
            <KeyboardDoubleArrowRightIcon id="next-icon" />
          </div>
          <div
            onClick={() => prevButtonFunction()}
            id={isReachTheStartData ? 'hide-element' : 'prev-btn'}
          >
            <KeyboardDoubleArrowLeftIcon id="prev-icon" />
          </div>
        </div>
      ) : null}
      <div className="mt-3" id="switch-btn">
        <FormControlLabel
          label="Privacy"
          value={isPrivacyOn}
          onChange={(e) => setIsPrivacyOn(!isPrivacyOn)}
          control={<SwitchUnstyled component={Root} {...label} />}
        />
      </div>
      {isPrivacyOn == false ? (
        <>
          <Grid className="mt-2" container rowSpacing={4} columnSpacing={{ xs: 2, sm: 2, md: 2 }}>
            <Grid
              item
              md={8}
              sm={12}
              xs={12}
              direction="column"
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              {' '}
              <div id="container" className="position-relative">
                <Grid container rowSpacing={4} columnSpacing={{ xs: 1, sm: 1, md: 1 }}>
                  <Grid
                    item
                    md={3}
                    sm={12}
                    xs={12}
                    direction="column"
                    style={{
                      display: 'flex',
                      marginTop: '5px',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <div id="card-bgc2">
                      <div id="tenant-image-holder2">
                        <img id="tenant-image" src={tenantData.picture} />
                      </div>
                    </div>
                  </Grid>
                  <Grid
                    className="ml-3"
                    item
                    md={8}
                    sm={12}
                    xs={12}
                    direction="column"
                    style={{
                      display: 'flex',
                      marginTop: '25px',
                      marginLeft: '50px',
                      justifyContent: 'left',
                      alignItems: 'left',
                    }}
                  >
                    <div>
                      <div>
                        <strong
                          className="d-inline-block pl-2"
                          style={{
                            fontSize: '20px',
                          }}
                        >
                          Allergies:
                        </strong>
                        {allergiesList.length === 0 ? (
                          <p className="d-inline-block pl-2"> No Known Allergy yet</p>
                        ) : null}
                        {allergiesList.map((item, key) => (
                          <p key={key} id="allergy-List" className="px-1 d-inline-block">
                            • {item.allergy}
                          </p>
                        ))}
                      </div>
                      <div>
                        <strong
                          className="d-inline-block pl-2"
                          style={{
                            fontSize: '20px',
                          }}
                        >
                          Diagnosis:
                        </strong>
                        {diagnosisData.length === 0 ? (
                          <p className="d-inline-block pl-2"> No Diagnosis Data yet</p>
                        ) : null}
                        {diagnosisData.map((item, key) => (
                          <p key={key} id="allergy-List" className="px-1 d-inline-block">
                            • {item.diagnosisName}
                          </p>
                        ))}
                      </div>
                    </div>
                  </Grid>
                  <Grid
                    className="position-relative"
                    item
                    md={12}
                    sm={12}
                    xs={12}
                    direction="column"
                    style={{
                      display: 'flex',
                      justifyContent: 'left',
                      paddingTop: '10px',
                      alignItems: 'left',
                    }}
                  >
                    <div id="hr-line" className="mx-auto">
                      {' '}
                    </div>
                    <div className="mt-2">
                      <strong
                        style={{
                          fontSize: '20px',
                          marginLeft: '50px',
                        }}
                      >
                        Profile
                      </strong>
                    </div>
                  </Grid>
                  <Grid
                    className=" p-0 mb-3 position-relative"
                    item
                    md={12}
                    sm={12}
                    xs={12}
                    direction="column"
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <div>
                      <div>
                        <HomeIcon /> <strong>Community :</strong> {tenantData.roomName}
                      </div>
                      <div>
                        <VolunteerActivismIcon /> <strong>Care Level :</strong>{' '}
                        {tenantData.careLevel}
                      </div>
                      <div>
                        <DateRangeIcon />
                        <strong>Age :</strong> {getTheAge(tenantData.birthday)}
                      </div>
                      <div>
                        <EditLocationAltIcon />
                        <strong>Address :</strong> {tenantData.address}
                      </div>
                      <div>
                        <PersonSearchIcon /> <strong>Status :</strong> {tenantData.status}
                      </div>
                      <div className="mt-2">
                        <Box id="drop-Drown2" sx={{ minWidth: 120 }}>
                          <FormControl>
                            <InputLabel variant="standard" htmlFor="uncontrolled-native">
                              Status
                            </InputLabel>
                            <NativeSelect
                              value={statusInput}
                              defaultValue={tenantData.status}
                              onChange={(e) => setStatusInput(e.target.value)}
                            >
                              <option hidden styles={{ fontSize: '8px' }}>
                                change status
                              </option>
                              <option value="Cancel Move in">Cancel Move in</option>
                              <option value="Move Out">Move Out</option>
                              <option value="On Notice">On Notice</option>
                              <option value="Start LOA">Start LOA</option>
                              <option value="Put On Waitlist">Put On Waitlist</option>
                              <option value="Unit/Care Level Change">Unit/Care Level Change</option>
                              <option value="Schedule Unit Transfer">Schedule Unit Transfer</option>
                            </NativeSelect>
                          </FormControl>
                        </Box>
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Grid
              item
              className="position-relative"
              md={4}
              sm={12}
              xs={12}
              direction="column"
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <div id="container2" className="position-relative">
                <div className="mt-2">
                  <strong
                    style={{
                      fontSize: '20px',
                      marginLeft: '20px',
                    }}
                  >
                    <FontAwesomeIcon icon={faBell} style={{ fontSize: '25px', color: '#e55353' }} />{' '}
                    Notification
                  </strong>
                  <div id="reminder-holder" className="position-relative">
                    <div id="reminder-div">
                      <p className="text-right my-0">
                        {' '}
                        <FontAwesomeIcon
                          icon={faTriangleExclamation}
                          style={{ fontSize: '18px', color: '#f9b115' }}
                        />{' '}
                        This needs an attention.
                      </p>
                      <p className="text-left">Sample reminder item number one</p>
                      <div className="mb-3">
                        <Button id="btn" variant="contained">
                          View
                        </Button>
                      </div>
                    </div>
                    <div className="mt-5">
                      <p className="text-right my-0">
                        {' '}
                        <FontAwesomeIcon
                          icon={faTriangleExclamation}
                          style={{ fontSize: '18px', color: '#f9b115' }}
                        />{' '}
                        This needs an attention.
                      </p>
                      <p className="text-left">Sample reminder item number two</p>
                      <div className="mb-3">
                        <Button id="btn" variant="contained">
                          View
                        </Button>
                      </div>
                    </div>
                    <div className="mt-5">
                      <p className="text-right my-0">
                        {' '}
                        <FontAwesomeIcon
                          icon={faTriangleExclamation}
                          style={{ fontSize: '18px', color: '#f9b115' }}
                        />{' '}
                        This needs an attention.
                      </p>
                      <p className="text-left">Sample reminder item number three</p>
                      <div className="mb-3">
                        <Button id="btn" variant="contained">
                          View
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>

          <div id="container" className="position-relative pb-5 mt-3">
            <Box className="mt-3" sx={{ width: '100%' }}>
              <Box
                className="mx-auto"
                sx={{ borderBottom: 1, borderColor: 'divider', width: '95%' }}
              >
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                  <Tab label="Clinical" {...a11yProps(0)} />
                  <Tab label="Profile" {...a11yProps(1)} />
                  {/* <div>
                    <Button
                      id="basic-button"
                      className="profile-dropdown-menu"
                      aria-controls={open ? 'basic-menu' : undefined}
                      aria-haspopup="true"
                      aria-expanded={open ? 'true' : undefined}
                      onClick={handleClick}
                    >
                      Profile <KeyboardArrowRightIcon />
                    </Button>
                    <Menu
                      id="basic-menu"
                      anchorEl={anchorEl}
                      open={open}
                      onClose={handleClose}
                      MenuListProps={{
                        'aria-labelledby': 'basic-button',
                      }}
                    >
                      <MenuItem onClick={(e) => handleChange(e, 1)}>Basic Info</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 4)}>Contacts</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 5)}>Insurance Information</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 6)}>Healthcare Directives</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 7)}>Routines</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 8)}>Medical History</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 9)}>Surgeries</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 10)}>Disablities</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 11)}>Miscellaneous</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 12)}>Digital Documents</MenuItem>
                      <MenuItem onClick={(e) => handleChange(e, 13)}>Digital Documents</MenuItem>
                    </Menu>
                  </div> */}

                  <Tab label="Reports" {...a11yProps(2)} onClick={(e) => resetActiveTab()} />
                  <Tab label="e-Documents" {...a11yProps(3)} onClick={(e) => resetActiveTab()} />
                </Tabs>
              </Box>
              <TabPanel value={value} index={0}>
                {/* cards code start here */}
                <CContainer>
                  <CRow xs={{ gutterY: 3 }}>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(1)}>
                          <CardContent className="position-relative">
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faHandDots} style={{ color: '#e55353' }} />{' '}
                                Allergies
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {allergiesList.length > 0 ? (
                                <>
                                  {' '}
                                  <h5> {allergiesList[allergiesList.length - 1].allergy}</h5>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest allergy data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(allergiesList[allergiesList.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(2)}>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faFaceGrin} style={{ color: '#2eb85c' }} />{' '}
                                Behavior
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {behaviorData.length > 0 ? (
                                <>
                                  {' '}
                                  <h5> {behaviorData[behaviorData.length - 1].behavior}</h5>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest behavior data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(behaviorData[behaviorData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(3)}>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faClipboard} style={{ color: '	#3399ff' }} />{' '}
                                Daily log
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {dailylogData.length > 0 ? (
                                <>
                                  {' '}
                                  <h5> {dailylogData[dailylogData.length - 1].title}</h5>
                                  <p>{dailylogData[dailylogData.length - 1].note}</p>
                                  <p>
                                    Created by: {dailylogData[dailylogData.length - 1].createdBy}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest log data of the resident
                                  </small>
                                  <p>
                                    Issued Date:{' '}
                                    {formatDate(dailylogData[dailylogData.length - 1].date)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(4)}>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faUserDoctor} style={{ color: '#f9b115' }} />{' '}
                                Diagnosis
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {diagnosisData.length > 0 ? (
                                <>
                                  {' '}
                                  <h5> {diagnosisData[diagnosisData.length - 1].diagnosisName}</h5>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest diagnosis data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(diagnosisData[diagnosisData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faBurger} style={{ color: '	#f9b115' }} />{' '}
                                Dietary
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {dietaryData.length > 0 ? (
                                <>
                                  {' '}
                                  <h5> {dietaryData[dietaryData.length - 1].behavior}</h5>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest behavior data of the resident
                                  </small>
                                  <p>
                                    Started Date:{' '}
                                    {formatDate(dietaryData[dietaryData.length - 1].startDate)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(5)}>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon
                                  icon={faPersonFalling}
                                  style={{ color: '#4f5d73' }}
                                />{' '}
                                Incident
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {incidentReportList.length > 0 ? (
                                <>
                                  {' '}
                                  <h5>
                                    {' '}
                                    {
                                      incidentReportList[incidentReportList.length - 1]
                                        .incidentDescription
                                    }
                                  </h5>
                                  <p>
                                    Location:{' '}
                                    {incidentReportList[incidentReportList.length - 1].location}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest incident data of the resident
                                  </small>
                                  <p>
                                    Date Happened:{' '}
                                    {formatDate(
                                      incidentReportList[incidentReportList.length - 1].date,
                                    )}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea onClick={() => changeActiveTab(6)}>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faHeartPulse} style={{ color: '#e55353' }} />{' '}
                                Vitals
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              {vitalData.length > 0 ? (
                                <>
                                  {' '}
                                  <p>
                                    Blood Pressure: {vitalData[vitalData.length - 1].bloodPressure}
                                  </p>
                                  <p>
                                    Body Temperature: {vitalData[vitalData.length - 1].bodyTemp}
                                  </p>
                                  <p>Pulse Rate: {vitalData[vitalData.length - 1].pulseRate}</p>
                                  <p>
                                    Respiration Rate:{' '}
                                    {vitalData[vitalData.length - 1].respirationRate}
                                  </p>
                                  <small style={{ fontStyle: 'italic' }}>
                                    Latest Vital data of the resident
                                  </small>
                                  <p>
                                    Date Input: {formatDate(vitalData[vitalData.length - 1].date)}
                                  </p>
                                </>
                              ) : (
                                <p>No Available Data</p>
                              )}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                  </CRow>
                </CContainer>
              </TabPanel>
              <TabPanel value={value} index={1}>
                <CContainer>
                  <CRow xs={{ gutterY: 3 }}>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent className="position-relative">
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faCircleInfo} style={{ color: '#e55353' }} />{' '}
                                Basic Info
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faPhone} style={{ color: '#2eb85c' }} />{' '}
                                Contacts
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faHospital} style={{ color: '	#3399ff' }} />{' '}
                                Insurance Information
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon
                                  icon={faHospitalUser}
                                  style={{ color: '#f9b115' }}
                                />{' '}
                                Health Care Directives
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon
                                  icon={faCalendarCheck}
                                  style={{ color: '	#f9b115' }}
                                />{' '}
                                Routines
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faCrutch} style={{ color: '#4f5d73' }} />{' '}
                                Disabilities
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon
                                  icon={faPeopleGroup}
                                  style={{ color: '#e55353' }}
                                />{' '}
                                Miscellaneous
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                    <CCol lg={3} md={6}>
                      <Card
                        id="card-holder"
                        className="mx-auto"
                        style={{ width: 'auto', height: '16rem' }}
                      >
                        <CardActionArea>
                          <CardContent>
                            <div id="card-txt-title">
                              {' '}
                              <Typography
                                className="title"
                                gutterBottom
                                variant="h5"
                                component="div"
                              >
                                <FontAwesomeIcon icon={faFilePdf} style={{ color: '#e55353' }} />{' '}
                                Digital Documents
                              </Typography>
                            </div>
                            <Typography
                              className="mt-5 pb-5"
                              variant="body2"
                              color="text.secondary"
                            >
                              <p>No Available Data</p>
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </CCol>
                  </CRow>
                </CContainer>
              </TabPanel>
              <TabPanel value={value} index={2}>
                Reports
              </TabPanel>
              <TabPanel value={value} index={3}>
                e-Documents
              </TabPanel>
              <TabPanel value={value} index={4}>
                Contacts
              </TabPanel>
              <TabPanel value={value} index={5}>
                Insurance
              </TabPanel>
              <TabPanel value={value} index={6}>
                Healthcare
              </TabPanel>
              <TabPanel value={value} index={7}>
                Routines
              </TabPanel>
              <TabPanel value={value} index={8}>
                Medical History
              </TabPanel>
              <TabPanel value={value} index={9}>
                Surgeries
              </TabPanel>
              <TabPanel value={value} index={10}>
                Disabilities
              </TabPanel>
              <TabPanel value={value} index={11}>
                Miscellaneous
              </TabPanel>
              <TabPanel value={value} index={12}>
                Digital Documents
              </TabPanel>
            </Box>
          </div>
          {activeTab == 1 ? (
            <div className="mx-auto">
              {' '}
              <Allergies />
            </div>
          ) : activeTab == 2 ? (
            <div className="mx-auto">
              <Behavior />
            </div>
          ) : activeTab == 3 ? (
            <div className="mx-auto">
              <Dailylog />
            </div>
          ) : activeTab == 4 ? (
            <div className="mx-auto">
              <Diagnosis />
            </div>
          ) : activeTab == 5 ? (
            <div className="mx-auto">
              <Incident />
            </div>
          ) : activeTab == 6 ? (
            <div className="mx-auto">
              <Vital />
            </div>
          ) : null}
        </>
      ) : (
        <div id="tenant-profile-loader">
          <ContentLoader
            speed={2}
            width={1100}
            height={300}
            viewBox="0 0 476 124"
            backgroundColor={'#3333'}
            foregroundColor={'#999'}
          >
            <rect x="97" y="56" rx="3" ry="3" width="410" height="6" />
            <rect x="97" y="75" rx="3" ry="3" width="380" height="6" />
            <rect x="97" y="90" rx="3" ry="3" width="178" height="6" />
            <rect x="4" y="16" rx="0" ry="0" width="87" height="83" />
            <rect x="97" y="18" rx="3" ry="3" width="380" height="6" />
            <rect x="97" y="37" rx="3" ry="3" width="217" height="6" />
          </ContentLoader>
          <ContentLoader
            speed={2}
            width={1150}
            height={250}
            viewBox="0 0 476 124"
            backgroundColor={'#3333'}
            foregroundColor={'#999'}
          >
            <rect x="53" y="112" rx="0" ry="0" width="0" height="5" />
            <rect x="5" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="52" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="99" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="146" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="193" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="240" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="287" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="334" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="381" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="428" y="2" rx="0" ry="0" width="36" height="36" />
            <rect x="9" y="52" rx="0" ry="0" width="458" height="8" />
            <rect x="10" y="72" rx="0" ry="0" width="266" height="5" />
            <rect x="10" y="88" rx="0" ry="0" width="357" height="6" />
          </ContentLoader>
        </div>
      )}

      {/* <Dietary /> */}

      <Row>
        {/*<div style={{ display: "none" }}>
              <FaceSheet ref={componentRef} />
            </div> */}

        {/*insert action button tab here*/}

        <Col md="12" className="d-flex justify-content-center">
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </Col>
      </Row>
    </div>
  )
}

export default TenantProfile
