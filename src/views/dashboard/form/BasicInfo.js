import React, { useState, useEffect } from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
// axios api
import api from '../../../api/api'
import '../../../App.css'

function BasicInfo() {
  // useState for input field
  const [firstName, setFirstName] = useState('')
  const [middleName, setMiddleName] = useState('')
  const [lastName, setLastName] = useState('')
  const [resId, setResId] = useState('')
  const [uniqId, setUniqId] = useState('')
  const [roomName, setRoomName] = useState('')
  const [careLevel, setCareLevel] = useState('')

  let tenantId = localStorage.getItem('tenantId')

  const fetchTenant = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    await api
      .get(`/tenants/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setInitialValue(res.data)
      })
  }

  const setInitialValue = (data) => {
    setFirstName(data.firstName)
    setMiddleName(data.middleName)
    setLastName(data.lastName)
    setResId(data._id.slice(0, 10))
    setUniqId('0000' + data._id.slice(0, 10))
    setRoomName(data.roomName)
    setCareLevel(data.careLevel)
  }

  useEffect(() => {
    fetchTenant()
  }, [tenantId])

  return (
    <div>
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { m: 1, width: '25ch' },
        }}
        noValidate
        autoComplete="off"
      >
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Resident ID"
            id="outlined-size-small"
            value={resId}
            size="small"
            inputProps={{ readOnly: true }}
          />
          <TextField
            label="Unique Identifier"
            id="outlined-size-small"
            value={uniqId}
            readonly
            size="small"
          />
          <TextField label="Resident Code" id="outlined-size-small" readonly size="small" />
        </div>
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Account ID"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="First Name"
            id="outlined-size-small"
            value={firstName}
            size="small"
            inputProps={{ readOnly: true }}
          />
          <TextField
            label="Middle Name"
            id="outlined-size-small"
            value={middleName}
            size="small"
            inputProps={{ readOnly: true }}
          />
        </div>
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Last Name"
            id="outlined-size-small"
            value={lastName}
            size="small"
            inputProps={{ readOnly: true }}
          />
          <TextField
            label="Preferred Name"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Move In Date"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
        </div>
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Unit"
            id="outlined-size-small"
            value={roomName}
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Care Level"
            id="outlined-size-small"
            value={careLevel}
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Contract Type"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
        </div>
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Place of Birth"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Marital Status"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Race"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
        </div>
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Veteran Number"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Affinity"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Languages"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
        </div>
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Eye color"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Hair color"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Identifying marks"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
        </div>
        <div className="d-flex justify-content-center mb-2">
          <TextField
            label="Church Name/Affiliation"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
          <TextField
            label="Nursing Station/Neighborhood"
            id="outlined-size-small"
            inputProps={{ readOnly: true }}
            size="small"
          />
        </div>
      </Box>
    </div>
  )
}

export default BasicInfo
