import React, { useState, useEffect, useContext, useRef } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import '../../../App.css'
import AddIcon from '@mui/icons-material/Add'

// global variable
import AppContext from '../../../AppContext'
// axios api
import api from '../../../api/api'
// toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function EditBehaviorForm(data) {
  console.log(data)
  const {
    behaviorData,
    setBehaviorData,
    allergiesList,
    setAllergiesList,
    dialogClose,
    setDialogClose,
  } = useContext(AppContext)
  // State for all the input field
  const [behavior, setBehavior] = useState('')
  const [startDate, setStartDate] = useState('')
  const [location, setLocation] = useState('')
  const [alterable, setAlterable] = useState('')
  const [riskTo, setRiskTo] = useState('')
  const [times, setTimes] = useState('')
  const [triggerBy, setTriggerBy] = useState('')
  const [review, setReview] = useState('')

  // for Initial data from the edited data
  useEffect(() => {
    setBehavior(data.data.behavior)
    setLocation(data.data.location)
    setAlterable(data.data.alterable)
    setRiskTo(data.data.riskTo)
    setTimes(data.data.times)
    setTriggerBy(data.data.triggerBy)
    setReview(data.data.review)
    checkDates()
  }, [])

  const checkDates = () => {
    if (
      data.data.startDate == '' ||
      data.data.startDate == undefined ||
      data.data.startDate == null
    ) {
      setStartDate('')
    } else {
      let startDateInput = new Date(data.data.startDate)
      let newDate = startDateInput.toISOString().substr(0, 10)
      setStartDate(newDate)
    }

    // if (data.data.endDate == '' || data.data.endDate == undefined || data.data.endDate == null) {
    //   setEndDate('')
    // } else {
    //   let startDateInput = new Date(data.data.endDate)
    //   let newDate = startDateInput.toISOString().substr(0, 10)
    //   setEndDate(newDate)
    // }
  }

  const firstUpdate = useRef(true)

  const updateBehaviorData = () => {
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    const input = {
      behavior: behavior,
      startDate: startDate,
      location: location,
      alterable: alterable,
      riskTo: riskTo,
      times: times,
      triggerBy: triggerBy,
      review: review,
    }

    if (firstUpdate.current) {
      firstUpdate.current = false
      return
    } else {
      // api call for adding new tenant
      api
        .put(`/tenants/behavior/${tenantId}/update/${data.data._id}`, input, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((result) => {
          // updating the College student on it's state to instantly apply the changes without refreshing the page
          behaviorData.forEach((item) => {
            if (item._id === data.data._id) {
              item.behavior = behavior
              item.startDate = startDate
              item.location = location
              item.alterable = alterable
              item.riskTo = riskTo
              item.times = times
              item.triggerBy = triggerBy
              item.review = review
            }
          })

          setBehaviorData([...behaviorData])

          // adding history to database
          const input2 = {
            title: `Edited ${tenantName}'s diagnosis data on ${behavior}`,
            tenantName: tenantName,
            tenantId: tenantId,
            userName: name,
          }
          api
            .post(`/history/create/`, input2, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((result) => {})
            .catch((err) => {
              console.error(err)
            })
        })
        .catch((err) => {
          console.log(err)
        })

      toast.success('Updated Successfully', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      })
    }
  }

  useEffect(() => {
    const timeoutId = setTimeout(() => updateBehaviorData(), 2000)
    return () => clearTimeout(timeoutId)
  }, [behavior, startDate, location, alterable, riskTo, times, triggerBy, review])

  return (
    <div>
      <Form id="form">
        <Row>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              name="sportsCode"
              required
              value={behavior}
              onChange={(e) => setBehavior(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Behavior</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="date"
              name="date"
              required
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label htmlFor="date">Start Date</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem ">
            <Form.Control
              type="text"
              required
              value={location}
              onChange={(e) => setLocation(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Location</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              value={alterable}
              onChange={(e) => setAlterable(e.target.value)}
              placeholder=" "
              className="formItem mt-3 form__input"
            />
            <label>Alterable</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={riskTo}
              onChange={(e) => setRiskTo(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Risk To</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={times}
              onChange={(e) => setTimes(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Times</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={triggerBy}
              onChange={(e) => setTriggerBy(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Trigger By</label>
          </Col>
          <Col md="6" sm="8" className="mx-auto mb-3 colItem">
            <Form.Control
              type="text"
              placeholder=" "
              required
              value={review}
              onChange={(e) => setReview(e.target.value)}
              className="formItem mt-3 form__input"
            />
            <label>Review</label>
          </Col>
          <Col md="12" className="d-flex justify-content-center">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default EditBehaviorForm
