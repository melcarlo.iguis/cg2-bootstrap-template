import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../AppContext'

import Box from '@mui/material/Box'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import NativeSelect from '@mui/material/NativeSelect'
import { DataGrid } from '@mui/x-data-grid'

// axios api
import api from '../../../api/api'
import Swal from 'sweetalert2'

import Popup from '../popup/Popup'
import EditPopup from '../popup/EditPopup'
import AddNewDiagnosis from '../form/AddNewDiagnosis'
import EditDiagnosisForm from '../form/EditDiagnosisForm'
// Icons
import SearchIcon from '@mui/icons-material/Search'
import InputBase from '@material-ui/core/InputBase'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import AttachmentIcon from '@mui/icons-material/Attachment'
import EditIcon from '@mui/icons-material/Edit'

function Diagnosis() {
  // global variables
  const { setDiagnosisData, diagnosisData } = useContext(AppContext)
  // state for search word
  const [wordEntered, setWordEntered] = useState('')
  const [isFetchDone, setIsFetchDone] = useState(false)
  let token = localStorage.getItem('token')

  const formatDate = (string) => {
    console.log(string)
    if (string === undefined) {
      return ''
    } else {
      let date = new Date(string)
      const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
      const day = date.getUTCDate()
      const year = date.getUTCFullYear()

      let fullDate = month + ' ' + day + ', ' + year
      return fullDate
    }
  }

  // for filtering the course data based on department named
  // useEffect(() => {

  // 	let token = localStorage.getItem('token');
  //     let tenantId = localStorage.getItem('tenantId');
  //     api.get(`/tenants/allergy/${tenantId}/fetch` , {
  //         headers:{
  //           'Authorization' : `Bearer ${token}`
  //         }
  //       }).then(res => {

  // 		setAllergiesList(res.data)
  // 		console.log(res)
  // 		if(allergyTypeInput !== "all"){
  // 			const newFilter = res.data.filter((value) =>{

  // 			return value.allergyType.toLowerCase().includes(allergyTypeInput.toLowerCase())
  // 			})

  // 			setAllergiesList(newFilter)
  // 			console.log(newFilter)

  // 		}
  // 	})

  // }, [allergyTypeInput])

  // code for fetching allergy
  useEffect(() => {
    fetchDiagnosisData()
  }, [])

  const fetchDiagnosisData = () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')
    api
      .get(`/tenants/diagnosis/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setDiagnosisData(res.data)
        setIsFetchDone(true)
      })
  }

  // function for search input
  const handleFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    // let token = localStorage.getItem('token');
    // let tenantId = localStorage.getItem('tenantId');

    // await api.get(`/tenants/allergy/${tenantId}/fetch` , {
    //        headers:{
    //          'Authorization' : `Bearer ${token}`
    //        }
    //      }).then(res=>{
    //     	setAllergiesList(res.data)

    // 	const newFilter = res.data.filter((value) =>{

    // 		return value.allergy.toLowerCase().includes(searchWord.toLowerCase())
    // 	})
    // 	setAllergiesList(newFilter)

    //    })
  }

  const deleteDiagnosisData = (e, id, diagnosisName) => {
    e.preventDefault()

    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .put(`/tenants/diagnosis/${tenantId}/delete/${id}`)
          .then((result) => {
            const newArr = [...diagnosisData]

            newArr.forEach((item, index) => {
              if (item._id === id) {
                newArr.splice(index, 1)
              }
            })

            setDiagnosisData(newArr)
            Swal.fire('Deleted!', 'The Data has been deleted.', 'success')

            // adding history to database
            const input2 = {
              title: `Deleted ${tenantName}'s diagnosis data on ${diagnosisName}`,
              tenantName: tenantName,
              tenantId: tenantId,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {})
              .catch((err) => {
                console.error(err)
              })
          })
          .catch((err) => {
            Swal.fire({
              title: 'fail to delete',
              icon: 'error',
            })
          })
      }
    })
  }

  const columns = [
    {
      field: 'diagnosisName',
      headerName: 'Diagnosis Name',
      minWidth: 120,
      flex: 1,
    },
    {
      field: 'startDate',
      headerName: 'Start Date',
      minWidth: 150,
      sortable: false,
      flex: 1,
      valueGetter: (params) => {
        return formatDate(params.row.startDate)
      },
    },
    {
      field: 'endDate',
      headerName: 'End Date',
      minWidth: 220,
      flex: 1,
      sortable: false,
      valueGetter: (params) => {
        return formatDate(params.row.endDate)
      },
    },
    {
      field: 'attachment',
      headerName: 'Add Attachment',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div id="edit-Btn" className="btn">
          Add
          <AttachmentIcon />
        </div>
      ),
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>
            {' '}
            <EditDiagnosisForm data={params.row} />
          </EditPopup>
          <div
            id="delete-btn"
            className="btn d-flex"
            onClick={(e) => deleteDiagnosisData(e, params.row._id, params.row.diagnosisName)}
          >
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  return (
    <div id="container" className="mt-3">
      <div className="mb-5">
        <h1>Diagnosis</h1>
      </div>
      <Popup>
        <AddNewDiagnosis />
      </Popup>
      <Row>
        <div className="mt-3 mx-auto" id="datagrid-container">
          <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-5">
            {diagnosisData.length > 0 && isFetchDone == true ? (
              <DataGrid
                getRowId={(row) => row._id}
                rows={diagnosisData}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
              />
            ) : (
              <p>No Data Available</p>
            )}
          </Col>
        </div>
      </Row>
    </div>
  )
}

export default Diagnosis
