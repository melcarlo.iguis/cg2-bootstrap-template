import React, { useContext, useEffect, useState } from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import AppContext from '../../../AppContext'

import { DataGrid } from '@mui/x-data-grid'
import TextField from '@mui/material/TextField'
import Autocomplete from '@mui/material/Autocomplete'
// axios api
import api from '../../../api/api'
// MUI
import InputBase from '@material-ui/core/InputBase'
// icons
import SearchIcon from '@mui/icons-material/Search'

const cutDate = (string) => {
  let date = new Date(string)
  const month = date.toLocaleString('en-us', { month: 'long' }) /* June */
  const day = date.getUTCDate()
  const year = date.getUTCFullYear()

  const hour = date.getHours() % 12 || 12
  const min = date.getMinutes()
  const sec = date.getSeconds()

  var ampm = date.getHours() >= 12 ? 'PM' : 'AM'

  let fullDate = month + ' ' + day + ', ' + year + ' - ' + hour + ':' + min + ':' + sec + ' ' + ampm
  return fullDate
}


const getDateTime = (data) =>{
  return 
}

const columns = [
  {
    field: 'date',
    headerName: 'Date and Time',
    minWidth: 220,
    sortable: false,
    valueGetter: (params) => {
      return getDateTime(params.row.date)
    },
  },
  {
    field: 'title',
    headerName: 'Title',
    flex: 1,
    minWidth: 480,
  },
  {
    field: 'userName',
    headerName: 'User',
    flex: 1,
    minWidth: 80,
  },
]

function History() {
  // state for search word
  const [wordEntered, setWordEntered] = useState('')

  // global variables
  const {
    historyList,
    setHistoryList,
    allergiesList,
    setAllergiesList,
    currentTab,
    setCurrentTab,
  } = useContext(AppContext)

  const fetchTenantHistory = async () => {
    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get(`/history/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setHistoryList(res.data)
      })
  }

  useEffect(() => {
    fetchTenantHistory()
  }, [])

  // function for search input
  const handleFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    let token = localStorage.getItem('token')
    let tenantId = localStorage.getItem('tenantId')

    await api
      .get(`/history/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setHistoryList(res.data)

        const newFilter = res.data.filter((value) => {
          return (
            value.title.toLowerCase().includes(searchWord.toLowerCase()) ||
            value.userName.toLowerCase().includes(searchWord.toLowerCase())
          )
        })
        setHistoryList(newFilter)
      })
  }

  return (
    <div id="history-div">
      <Row>
        <Col className="mb-3" md="12">
          <div id="search-bar-container">
            <InputBase
              id="searchBar"
              placeholder="Search…"
              value={wordEntered}
              onChange={handleFilter}
              inputProps={{ 'aria-label': 'search' }}
              endAdornment={<SearchIcon style={{ fontSize: 25 }} className="pr-3" />}
            />
          </div>
        </Col>
        <Col md="12" className="mx-auto"></Col>
        <div id="datagrid-container">
          <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-3">
            <DataGrid
              getRowId={(row) => row._id}
              rows={historyList}
              columns={columns}
              pageSize={5}
              rowsPerPageOptions={[5]}
            />
          </Col>
        </div>
      </Row>
    </div>
  )
}

export default History
