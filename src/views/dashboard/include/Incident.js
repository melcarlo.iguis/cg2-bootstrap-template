import React, { useState, useEffect, useContext } from 'react'
import { Row, Col } from 'react-bootstrap'
import Popup from '../popup/Popup'
import AddIncidentReportForm from '../form/AddIncidentReportForm'
import EditIncidentReportForm from '../form/EditIncidentReportForm'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import EditPopup from '../popup/EditPopup'
// MUI
import InputBase from '@material-ui/core/InputBase'
import { Grid, Box, CardActionArea } from '@mui/material'
// icons
import SearchIcon from '@mui/icons-material/Search'
import { DataGrid } from '@mui/x-data-grid'
import Swal from 'sweetalert2'
// axios api
import api from '../../../api/api'
// global variable
import AppContext from '../../../AppContext'

function Incident() {
  // state for search word
  const [wordEntered, setWordEntered] = useState('')
  const [isFetchDone, setIsFetchDone] = useState(true)
  let tenantId = localStorage.getItem('tenantId')

  const { incidentReportList, setIncidentReportList } = useContext(AppContext)

  const fetchIncidentReport = () => {
    let token = localStorage.getItem('token')

    api
      .get(`/incident/${tenantId}/fetch`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setIncidentReportList(res.data)
        setIsFetchDone(true)
      })
  }

  console.log(incidentReportList)

  useEffect(() => {
    fetchIncidentReport()
  }, [tenantId])

  // function for search input
  const handleFilter = async (event) => {
    const searchWord = event.target.value
    setWordEntered(searchWord)

    // let token = localStorage.getItem('token');
    // let tenantId = localStorage.getItem('tenantId');

    // await api.get(`/history/${tenantId}/fetch` , {
    //        headers:{
    //          'Authorization' : `Bearer ${token}`
    //        }
    //      }).then(res=>{

    //     	setHistoryList(res.data)

    // 	const newFilter = res.data.filter((value) =>{

    // 		return value.title.toLowerCase().includes(searchWord.toLowerCase()) || value.userName.toLowerCase().includes(searchWord.toLowerCase())
    // 	})
    // 	setHistoryList(newFilter)

    //    })
  }

  const deleteIncidentReportData = (e, id, incident) => {
    e.preventDefault()
    let name = localStorage.getItem('name')
    let token = localStorage.getItem('token')
    let tenantName = localStorage.getItem('tenantName')
    let tenantId = localStorage.getItem('tenantId')

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .delete(`/incident/${tenantId}/delete/${id}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((result) => {
            console.log(result)
            const newArr = [...incidentReportList]

            newArr.forEach((item, index) => {
              if (item._id === id) {
                newArr.splice(index, 1)
              }
            })

            setIncidentReportList(newArr)
            Swal.fire('Deleted!', 'The Data has been deleted.', 'success')

            // adding history to database
            const input2 = {
              title: `Deleted ${tenantName}'s incident report data about ${incident}`,
              tenantName: tenantName,
              tenantId: tenantId,
              userName: name,
            }
            api
              .post(`/history/create/`, input2, {
                headers: {
                  Authorization: `Bearer ${token}`,
                },
              })
              .then((result) => {})
              .catch((err) => {
                console.error(err)
              })
          })
          .catch((err) => {
            Swal.fire({
              title: 'fail to delete',
              icon: 'error',
            })
          })
      }
    })
  }

  const cutDate = (string) => {
    let date = new Date(string)
    // const month = date.toLocaleString('en-us', { month: 'long' }); /* June */

    const month = date.getUTCMonth() + 1
    const day = date.getUTCDate()
    const year = date.getUTCFullYear()

    const hour = date.getHours() % 12 || 12
    const min = date.getMinutes()

    const newMins = min < 10 ? '0' + min : min
    var ampm = date.getHours() >= 12 ? 'PM' : 'AM'
    const newDay = day < 10 ? '0' + day : day

    let fullDate = month + '/' + newDay + '/' + year + ' - ' + hour + ':' + newMins + ' ' + ampm

    return fullDate
  }

  const columns = [
    {
      field: 'date',
      headerName: 'Date and Time',
      minWidth: 190,
      valueGetter: (params) => {
        return cutDate(params.row.date)
      },
    },
    {
      field: 'incidentDescription',
      headerName: 'Incident',
      width: 180,
    },

    {
      field: 'tenantName',
      headerName: 'Tenant',
      flex: 1,
      minWidth: 200,
    },
    {
      field: '_id',
      headerName: 'Control ID',
      minWidth: 220,
      sortable: false,
    },
    {
      field: 'action',
      headerName: 'Action',
      sortable: false,
      flex: 1,
      minWidth: 80,
      renderCell: (params) => (
        <div className="d-flex">
          <EditPopup>
            <EditIncidentReportForm data={params.row} />
          </EditPopup>
          <div
            id="delete-btn"
            className="btn d-flex"
            onClick={(e) => deleteIncidentReportData(e, params.row._id, params.row.description)}
          >
            <DeleteForeverIcon />
          </div>
        </div>
      ),
    },
  ]

  return (
    <div id="container" className="mt-3">
      <div className="mb-5">
        <h1>Incident</h1>
      </div>
      <Row>
        <Grid container rowSpacing={4} columnSpacing={{ xs: 1, sm: 1, md: 1 }}>
          <Grid item md={4} sm={10} direction="column" className="ml-5 pb-3">
            <Popup className="pb-5">
              <AddIncidentReportForm />
            </Popup>
          </Grid>

          {/*<InputBase
            placeholder="Search…"
            value={wordEntered}
            onChange={handleFilter}
            inputProps={{ 'aria-label': 'search' }}
            endAdornment={<SearchIcon style={{ fontSize: 50 }} className="pr-3" />}
            id="searchBar2"
          />*/}
        </Grid>
        <Col md="12" className="mx-auto"></Col>
        <div id="datagrid-container" className="mx-auto">
          <Col id="datagrid2" md="12" className="mx-auto mb-3 mt-3">
            {incidentReportList.length > 0 && isFetchDone == true ? (
              <DataGrid
                getRowId={(row) => row._id}
                rows={incidentReportList}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
              />
            ) : (
              <p>No Data Available</p>
            )}
          </Col>
        </div>
        <Col md="12" className="mx-auto"></Col>
      </Row>
    </div>
  )
}

export default Incident
