import React from 'react'
import { Grid, Box, CardActionArea } from '@mui/material'
import PropTypes from 'prop-types'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Typography from '@mui/material/Typography'

import BasicInfo from '../form/BasicInfo'

// tab panel
function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

function Profile() {
  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  const label = { componentsProps: { input: { 'aria-label': 'Demo switch' } } }

  return (
    <div>
      <Grid id="container2" container sm={12} md={12}>
        <Grid
          item
          md={2}
          sm={4}
          direction="column"
          style={{
            display: 'flex',
            marginTop: '25px',
            justifyContent: 'left',
            alignItems: 'left',
          }}
        ></Grid>
        <Grid item md={10} sm={8} direction="column">
          <TabPanel value={value} index={0}>
            <BasicInfo />
          </TabPanel>
          <TabPanel value={value} index={1}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={2}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={3}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={4}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={5}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={6}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={7}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={8}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={9}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={10}>
            Item Two
          </TabPanel>
        </Grid>
      </Grid>
    </div>
  )
}

export default Profile
